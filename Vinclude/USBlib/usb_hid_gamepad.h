/**
  ******************************************************************************
  * @file    usb_hid_gamepad.h
  *
  * @brief   ������������ ���� ������ HID USB Device.
  *
  *          �������, ������� 5 ������.
  *
  *          ����������: GCC ARM 4.9.3
  *          �����: Qt Creator 3.4.2
  *
  * @author  �����
  *             - ������� �������, lonie@niiet.ru
  *             - ������ ������, kolbov@niiet.ru
  * @date    21.09.2013
  *
  ******************************************************************************
  * @attention
  *
  * ������ ����������� ����������� ��������������� ���� ���ܻ, ��� �����-����
  * ��������, ���� ���������� ��� ���������������, ������� �������� ��������
  * �����������, ������������ �� ��� ����������� ���������� � ����������
  * ���������, �� �� ������������� ���. ������ ����������� �����������
  * ������������� ��� ��������������� ����� � ���������� ������ ��
  * �������������� �������������� ���������� � ��������, � ����� ��������� �����
  * �����������. �� � ����� ������ ������ ��� ��������������� �� �����
  * ��������������� �� �����-���� �����, �� ������ ��� ��������� �����, ���
  * �� ���� �����������, ��������� ��-�� ������������� ������������ �����������
  * ��� ���� �������� � ����������� ������������.
  *
  * <h2><center>&copy; 2016 ��� "�����"</center></h2>
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __USB_HID_GAMEPAD_H
#define __USB_HID_GAMEPAD_H

/* Includes ------------------------------------------------------------------*/
#include <USBlib/usb_dev.h>

/* Exported defines ----------------------------------------------------------*/
#define HID_DESCRIPTOR_TYPE 0x21
#define HID_CLASS_SPEC_RELEASE_NUMBER 0x0111

#define HID_REPORT_DESCRIPTOR 0x22

#define EP_DATA_IN 0x81
#define HID_IN_PACKET 4

#define HID_REQ_SET_IDLE 0x0A
#define HIR_REQ_GET_REPORT 0x01

/* Exported types ------------------------------------------------------------*/
typedef struct 
{
    uint8_t buttons1;
}hid_report_t;

/* Exported variables --------------------------------------------------------*/
extern USBDev_ClassCB_TypeDef USBDEV_HID_cb;

/* Exported functions prototypes ---------------------------------------------*/
uint32_t HID_SendReport(void);

#endif /* __USB_HID_GAMEPAD_H */
